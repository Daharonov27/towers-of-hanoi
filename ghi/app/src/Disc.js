
function Disc(props){
    let row = `row justify-content-md-center border border-danger ${props.selected === 1 ? "border-3" : ""}`
    let row2 = `row justify-content-md-center border border-warning ${props.selected === 2 ? "border-3" : ""}`
    let row3 = `row justify-content-md-center border border-primary ${props.selected === 3 ? "border-3" : ""}`
    const dict = {1: row, 2: row2, 3:row3};

    return(
        <div className="row justify-content-md-center">
            <div onClick={props.handleDiscClick} disc-number={props.disc} className={dict[props.disc]} style={{marginBottom: "0.2px", width: `${20 + (20 * props.disc)}%`}}>
                {props.disc}
            </div>
        </div>
    )
}

export default Disc;
