import { useState } from "react";
import _ from 'lodash'
import Column from "./Column";

function Game(){

    const [selected, setSelected] = useState(0);
    const [board, setBoard] = useState([[1, 2, 3],[],[]]);

    function handleDiscClick(event){
        const value = parseInt(event.target.getAttribute("disc-number"));


        if (selected !== value){
            setSelected((value));
        } else {
            setSelected(0);
        }

    }

    function handleColumnClick(event){
        if (selected === 0){
            return;
        }

        const clonedBoard = _.cloneDeep(board);
        const selectedColumn = event.target.getAttribute("column-index");

        for (let i = 0; i < clonedBoard.length; i++){
            const column = clonedBoard[i];

            if (_.includes(column, selected)){
                _.remove(column, function(disc){
                    return disc === selected;
                })
            }

        }

        if (clonedBoard[selectedColumn].length !== 0){
            const currentDisc = clonedBoard[selectedColumn]
            if (selected > currentDisc){
                return false;
            }
        }

        clonedBoard[selectedColumn].unshift(selected);
        setBoard(clonedBoard);
        setSelected(0);
        checkWin(clonedBoard);

    }

    function checkWin(clonedBoard){
        if(clonedBoard[2].length === 3){
            return true;
        }
        return false;
    }


    const successMessage = <div style={{marginRight: "60px"}}>Congrats you won!</div>


    return (
        <div className="container text-center" style={{marginTop: "100px"}}>
            {checkWin(board) && successMessage}
            <div className="row justify-content-md-center" style={{minHeight: "115px"}}>
                {board.map((column, i) => {
                    return (
                            <Column key={i} columnIndex={i} selected={selected} column={column} handleColumnClick={handleColumnClick} handleDiscClick={handleDiscClick}/>
                        )
                    })}
            </div>
        </div>
    );
}
export default Game;
