import Disc from "./Disc";

function Column(props){


    return(
        <div className="col align-self-end" style={{ marginRight: "65px"}}>
            {props.column.map(disc => {
                return(
                    <Disc key={disc} disc={disc} selected={props.selected} handleDiscClick={props.handleDiscClick}/>
                )
            })}
            <div onClick={props.handleColumnClick} column-index={props.columnIndex} style={{height: "30px", backgroundColor: "grey"}}></div>
        </div>
    );
}

export default Column;
